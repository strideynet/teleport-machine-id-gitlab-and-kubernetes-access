# Teleport Machine ID - GitLab and Kubernetes Access

Demo showing deployment to a Kubernetes cluster through Teleport using
Machine ID!

Setup:
- Add a Kubernetes cluster to Teleport (mines called gcp)
- Modify and deploy the contents of teleport/ for your repository/cluster
- Run the GitLab CI!